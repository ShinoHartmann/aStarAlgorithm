package shino.gsm.astar;

import shino.gsm.astar.state.State;
import shino.gsm.astar.state.StateComparator;

import java.util.*;

public class Puzzle {

    private List<Integer> checkedStates = new ArrayList<>();
    private State currentState;
    private int goalHash;
    private Queue<State> openStates = new PriorityQueue<>(new StateComparator());
    private int steps = 1;

    public Puzzle(Cell[][] initialCells) {
        currentState = new State(this, initialCells, 0);
        checkedStates.add(currentState.hashCode());
        goalHash = currentState.getGoalHash();
    }

    public void solve() {
        boolean solved = false;
        while(!solved) {
            showOutput();
            if(currentState.hashCode() == goalHash) {
                solved = true;
                System.out.println("Solved");
            } else {
                createNewOpenList();
                if(openStates.size() > 0) {
                    prepareNextIteration();
                } else {
                    System.out.println("Could not find the solution");
                    solved = true;
                }
            }
        }
    }

    private void showOutput() {
        System.out.println("Steps: " + steps++ + ", Hierarchy: " + currentState.getHierarchy() + ", Heuristic: " + currentState.getHeuristics() + ", ID: " + currentState.getId());
        System.out.println(currentState.toString());
    }

    private void createNewOpenList() {
        List<State> children = currentState.getChildren();
        addCheckedList(children);
        openStates.addAll(children);
    }

    private void addCheckedList(List<State> children) {
        for(State state: children) {
            checkedStates.add(state.hashCode());
        }
    }

    private void prepareNextIteration() {
        currentState = openStates.poll();
        System.out.println("|\nV");
    }

    public boolean isAlreadyChecked(State state) {
        return checkedStates.contains(state.hashCode());
    }
}
