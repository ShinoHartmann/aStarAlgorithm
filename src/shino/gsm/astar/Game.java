package shino.gsm.astar;

public class Game {

    private static Puzzle puzzle;

    public static void main(String[] args) {
        Cell[][] defaultGame = generateDefaultGame();
        puzzle = new Puzzle(defaultGame);
        puzzle.solve();
    }

    private static Cell[][] generateDefaultGame(){
        Cell[][] cells = new Cell[3][3];
        cells[0][0] = new Cell(2,1);
        cells[0][1] = new Cell(8,2);
        cells[0][2] = new Cell(3,3);
        cells[1][0] = new Cell(1,7);
        cells[1][1] = new Cell(6,8);
        cells[1][2] = new Cell(4,4);
        cells[2][0] = new Cell(7,0);
        cells[2][1] = new Cell(0,6);
        cells[2][2] = new Cell(5,5);
        return cells;
    }

    private static Cell[][] generateGame2(){
        Cell[][] cells = new Cell[3][3];
        cells[0][0] = new Cell(2,1);
        cells[0][1] = new Cell(8,2);
        cells[0][2] = new Cell(3,3);
        cells[1][0] = new Cell(1,8);
        cells[1][1] = new Cell(6,0);
        cells[1][2] = new Cell(4,4);
        cells[2][0] = new Cell(7,7);
        cells[2][1] = new Cell(0,6);
        cells[2][2] = new Cell(5,5);
        return cells;
    }
}
