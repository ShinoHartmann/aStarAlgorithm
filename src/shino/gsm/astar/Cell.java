package shino.gsm.astar;

public class Cell {

    private int value;
    private int goalValue;
    private int heuristic;

    public Cell (int value, int goalValue){
        this.value = value;
        this.goalValue = goalValue;
        heuristic = calculateHeuristic();
    }

    private int calculateHeuristic() {
        if(isOutOfPlace()) {
            return 1;
        } else {
            return 0;
        }
    }

    private boolean isOutOfPlace() {
        return !(value == goalValue) && value != 0;
    }

    public int getHeuristic() {
        return heuristic;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int getGoalValue() {
        return goalValue;
    }
}
