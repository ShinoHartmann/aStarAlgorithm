package shino.gsm.astar.state;

import shino.gsm.astar.Cell;
import shino.gsm.astar.Puzzle;
import java.util.ArrayList;
import java.util.List;

public class State {

    private static int startId = 0;
    private final int id = startId++;
    private final Cell[][] cells;
    private final int heuristics;
    private final Puzzle puzzle;
    private final int hierarchy;
    private final int blankCellX;
    private final int blankCellY;

    public State (Puzzle puzzle, Cell[][] cells, int hierarchy) {
        this.puzzle = puzzle;
        this.cells = cells;
        this.hierarchy = hierarchy;
        heuristics = calculateHeuristic(hierarchy);
        int[] blankCellPosition = getBlankCellPosition();
        blankCellX = blankCellPosition[0];
        blankCellY = blankCellPosition[1];
    }

    private int[] getBlankCellPosition() {
        int blankPosition[] = new int[2];
        for(int x=0; x < 3; x++) {
            for(int y=0; y < 3; y++) {
                if(cells[x][y].getValue() == 0) {
                    blankPosition[0] = x;
                    blankPosition[1] = y;
                }
            }
        }
        return blankPosition;
    }

    public List<State> getChildren() {
        List<State> children = new ArrayList<>();
        switch(blankCellX) {
            case 0:
                addStateIfNotCheckedYet(children, moveBlankCellDown());
                break;
            case 1:
                addStateIfNotCheckedYet(children, moveBlankCellDown());
                addStateIfNotCheckedYet(children, moveBlankCellUp());
                break;
            case 2:
                addStateIfNotCheckedYet(children, moveBlankCellUp());
                break;
        }
        switch(blankCellY){
            case 0:
                addStateIfNotCheckedYet(children, moveBlankCellRight());
                break;
            case 1:
                addStateIfNotCheckedYet(children, moveBlankCellRight());
                addStateIfNotCheckedYet(children, moveBlankCellLeft());
                break;
            case 2:
                addStateIfNotCheckedYet(children, moveBlankCellLeft());
                break;
        }
        return children;
    }

    private void addStateIfNotCheckedYet(List<State> list, State state) {
        if(!puzzle.isAlreadyChecked(state)) {
            list.add(state);
        }
    }

    private State moveBlankCellUp() {
        return moveBlankCell(-1, 0);
    }

    private State moveBlankCellDown() {
        return moveBlankCell(1, 0);
    }

    private State moveBlankCellLeft() {
        return moveBlankCell(0, -1);
    }

    private State moveBlankCellRight() {
        return moveBlankCell(0, 1);
    }

    private State moveBlankCell(int xStep, int yStep) {
        Cell[][] newCells = createNewCells();
        int nextXPos = blankCellX + xStep;
        int nextYPos = blankCellY + yStep;
        int nextStateValue = newCells[nextXPos][nextYPos].getValue();
        // swap the values of the blank cell and the neighbour cell
        newCells[blankCellX][blankCellY].setValue(nextStateValue);
        newCells[nextXPos][nextYPos].setValue(0);
        return new State(puzzle, newCells, hierarchy +1);
    }

    private Cell[][] createNewCells() {
        Cell[][] newCells = new Cell[3][3];
        for (int i = 0; i < cells.length; i++) {
            for(int j = 0; j < cells.length; j++) {
                newCells[i][j] = new Cell(cells[i][j].getValue(), cells[i][j].getGoalValue());
            }
        }
        return newCells;
    }

    private int calculateHeuristic(int steps) {
        int heuristic = steps;
        for(Cell[] horizontalCells: cells) {
            for(Cell cell: horizontalCells) {
                heuristic += cell.getHeuristic();
            }
        }
        return heuristic;
    }

    private String toString(GetCellValue getValueFunction) {
        StringBuilder output = new StringBuilder();
        String value;
        for(int i=0; i < 3; i++) {
            for(int j=0; j < 3; j++) {
                if(getValueFunction.get(cells[i][j]) == 0) {
                    value = " ";
                } else {
                    value = String.valueOf(getValueFunction.get(cells[i][j]));
                }
                appendString(output, value);
            }
            output.append("\n");
        }
        return output.toString();
    }

    @Override
    public String toString() {
        return toString(new GetCellValue());
    }

    public int getGoalHash() {
        return toString(new GetCellGoalValue()).hashCode();
    }

    private void appendString(StringBuilder output, String value) {
        output.append("[").append(value).append("]");
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof State)) {
            return false;
        }
        State stateToCompare = (State) obj;
        return hashCode() == stateToCompare.hashCode();
    }

    public int getHeuristics() {
        return heuristics;
    }

    public int getHierarchy() {
        return hierarchy;
    }

    public Integer getId() {
        return id;
    }

    private static class GetCellValue {
        public int get(Cell cell) {
            return cell.getValue();
        }
    }

    private static class GetCellGoalValue extends GetCellValue {
        public int get(Cell cell) {
            return cell.getGoalValue();
        }
    }
}
