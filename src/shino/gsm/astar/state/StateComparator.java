package shino.gsm.astar.state;

import java.util.Comparator;

public class StateComparator implements Comparator<State> {

    @Override
    public int compare(State stateOne, State stateTwo) {
        int heuristicOne = stateOne.getHeuristics();
        int heuristicTwo = stateTwo.getHeuristics();

        if (heuristicOne > heuristicTwo) {
            return 1;
        } else if (heuristicOne == heuristicTwo) {
            return -1 * stateOne.getId().compareTo(stateTwo.getId());
        } else {
            return -1;
        }
    }

}
